﻿namespace test_parser_tests
{
    using System.Linq;
    using NUnit.Framework;
    using text_parser.Infrastructure;
    using text_parser.Infrastructure.contracts;
    using text_parser.Utilities;
    
    [TestFixture]
    public class when_asked_to_process_words
    {
        private string[] words;
        private IDictionaryManager dictionary_manager;

        [TestFixtureSetUp]
        public void setup()
        {
            dictionary_manager = new DictionaryManager();
            words = new string[] { "This", "is", "a", "test" };
        }

        [TestFixtureTearDown]
        public void TearDown()
        {
            dictionary_manager = null;
            words = null;
        }

        [Test]
        public void should_add_unique_words_to_dictionary()
        {
            words.ForAll(word => dictionary_manager.AddOrUpdate(word));
            Assert.Greater(dictionary_manager.GetAll().ToList().Count, 0);
        }

        [Test]
        public void should_remove_all_words_from_dictionary()
        {
            dictionary_manager.ClearAll();
            Assert.AreEqual(dictionary_manager.GetAll().ToList().Count, 0);
        }

        [Test]
        public void should_increment_the_count_when_duplicate_word_is_found()
        {
            words = new string[] { "test", "test" };
            dictionary_manager.ClearAll();
            words.ForAll(word => dictionary_manager.AddOrUpdate(word));
            var entry = dictionary_manager.FindBy("test");
            Assert.GreaterOrEqual(entry.Value, 2);
        }
    }
}
