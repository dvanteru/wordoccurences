﻿namespace test_parser_tests
{
    using System.IO;
    using System.Text;
    using NUnit.Framework;
    using Rhino.Mocks;
    using text_parser.FileTasks.Processor.contracts;
    using text_parser.Repository.Contracts;

    [TestFixture]
    public class when_asked_for_stream_parser
    {
        private MemoryStream memory_stream;
        private IFileRepository file_repository;
        private const string TEST_STRING = "This is the test string";
        private const string FILE_PATH = "test.txt";
        private IProcessor processor;

        [SetUp]
        public void setup()
        {
            memory_stream = new MemoryStream(ASCIIEncoding.Default.GetBytes("Your string here"));
            file_repository = MockRepository.GenerateMock<IFileRepository>();
            processor = MockRepository.GenerateMock<IProcessor>();
        }

        [Test]
        public void should_return_the_string()
        {
            file_repository.Stub(x => x.OpenFile(FILE_PATH)).Return(memory_stream);
            processor.Expect(x => x.Process(new string[]{ TEST_STRING }));
        }
    }
}
