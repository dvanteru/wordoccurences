﻿namespace test_parser_tests
{
    using System.Linq;
    using text_parser.PrimeTasks;
    using text_parser.PrimeTasks.contracts;
    using NUnit.Framework;

    [TestFixture]
    public class when_asked_to_generate_prime_numbers
    {
        [Test]
        public void should_generate_prime_numbers_for_max_value()
        {
            IGeneratePrimes generate_primes = new GeneratePrimes();
            generate_primes.CalculatePrimesUntil(50000000);
            Assert.Greater(GeneratePrimes.GetAllPrimeNumbers().Count(), 1);
        }
    }
}
