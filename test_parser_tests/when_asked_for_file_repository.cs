﻿namespace test_parser_tests
{
    using System.IO;
    using Rhino.Mocks;
    using NUnit.Framework;
    using text_parser.Repository;
    using text_parser.Repository.Contracts;

    [TestFixture]
    public class when_asked_for_file_repository
    {
        string file_path = string.Empty;
        MemoryStream memory_stream = new MemoryStream();
        
        [Test]
        public void should_be_able_to_open_file()
        {
            var file_repository = MockRepository.GenerateMock<IFileRepository>();
            file_repository.Stub(x => x.OpenFile(file_path)).Return(memory_stream);
            Assert.IsNotNull(memory_stream);
        }

        /// <summary>
        /// This is the integration test. Should be moved into a different test library.
        /// </summary>
        [Test]
        public void should_return_non_empty_memory_stream_when_file_is_provided()
        {
            var file_repository = new FileRepository();
            memory_stream = file_repository.OpenFile("test.txt");
            Assert.Greater(memory_stream.Length, 0);
        }
    }
}
