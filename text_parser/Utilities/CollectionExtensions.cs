﻿namespace text_parser.Utilities
{
    using System;

    public static class CollectionExtensions
    {
        public static void ForAll(this string[] words, Action<string> action)
        {
            foreach (var word in words)
            {
                action.Invoke(word);
            }
        }
    }
}