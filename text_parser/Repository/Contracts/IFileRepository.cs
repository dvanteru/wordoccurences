﻿namespace text_parser.Repository.Contracts
{
    using System.IO;

    public interface IFileRepository
    {
        MemoryStream OpenFile(string filePath);
    }
}