﻿namespace text_parser.Repository
{
    using System.IO;
    using text_parser.Repository.Contracts;

    public class FileRepository : IFileRepository
    {
        private MemoryStream memoryStream;

        public MemoryStream OpenFile(string file_path)
        {
            using (var file_stream = new FileStream(file_path, FileMode.Open, FileAccess.Read))
            {
                memoryStream = new MemoryStream();
                memoryStream = file_stream.CreateMemoryStream(ref memoryStream);
            }
            return memoryStream;
        }
    }
}