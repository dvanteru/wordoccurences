﻿namespace text_parser.Repository
{
    using System.IO;

    public static class Extensions
    {
        public static MemoryStream CreateMemoryStream(this FileStream fileStream, ref MemoryStream memoryStream)
        {
            var bytes = new byte[fileStream.Length];
            fileStream.Read(bytes, 0, (int)fileStream.Length);
            memoryStream.Write(bytes, 0, (int)fileStream.Length);
            return memoryStream;
        }
    }
}