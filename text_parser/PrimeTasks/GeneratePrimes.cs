﻿namespace text_parser.PrimeTasks
{
    using System;
    using System.Collections.Generic;
    using text_parser.PrimeTasks.contracts;

    public class GeneratePrimes : IGeneratePrimes
    {
        private static IList<int> primeNumbers;

        public GeneratePrimes()
        {
            primeNumbers = new List<int> {2, 3};
        }

        public void CalculatePrimesUntil(int maxSize)
        {
            var squareRoot = Math.Sqrt(maxSize);
            var isPrime = new bool[maxSize + 1];

            SieveOfAtkinsAlgorithmForPrimeNumbers(maxSize, squareRoot, ref isPrime);
            for (var n = 5; n <= maxSize; n+=2)
            {
                   if (isPrime[n])  primeNumbers.Add(n);
            }
        }

        public static IEnumerable<int> GetAllPrimeNumbers()
        {
            foreach (var primeNumber in primeNumbers)
            {
                yield return primeNumber;
            }
        }

        private static void SieveOfAtkinsAlgorithmForPrimeNumbers(int maxSize, double squareRoot, ref bool[] isPrime)
        {
            for (var x = 1; x <= squareRoot; x++)
            {
                for (var y = 1; y <= squareRoot; y++)
                {
                    var n = 4 * x * x + y * y;
                    if (n <= maxSize && (n % 12 == 1 || n % 12 == 5))
                        isPrime[n] ^= true;

                    n = 3 * x * x + y * y;
                    if (n <= maxSize && n % 12 == 7)
                        isPrime[n] ^= true;

                    n = 3 * x * x - y * y;
                    if (x > y && n <= maxSize && n % 12 == 11)
                        isPrime[n] ^= true;
                }
            }

            for (var n = 5; n <= squareRoot; n++)
            {
                if (isPrime[n])
                {
                    var square = n * n;
                    for (var k = square; k <= maxSize; k += square) isPrime[k] = false;
                }
            }
        }
    }
}