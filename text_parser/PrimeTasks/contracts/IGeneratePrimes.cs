﻿namespace text_parser.PrimeTasks.contracts
{
    public interface IGeneratePrimes
    {
        void CalculatePrimesUntil(int maxSize);
    }
}