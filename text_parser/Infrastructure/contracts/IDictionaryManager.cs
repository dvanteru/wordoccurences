﻿namespace text_parser.Infrastructure.contracts
{
    using System.Collections.Generic;

    public interface IDictionaryManager
    {
        void AddOrUpdate(string word);

        IEnumerable<KeyValuePair<string, int>> GetAll();

        KeyValuePair<string, int> FindBy(string word);

        void ClearAll();
    }
}