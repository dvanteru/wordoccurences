﻿namespace text_parser.Infrastructure
{
    using System.Collections.Generic;

    public static class DataExtensions
    {
        public static void IncrementCountByOneFor(this IDictionary<string, int> dictionary, string key)
        {
            int count;
            dictionary.TryGetValue(key, out count);
            dictionary[key] = count + 1;
        }
    }
}