﻿namespace text_parser.Infrastructure
{
    using System.Collections.Generic;
    using text_parser.Infrastructure.contracts;
    using System.Linq;

    public class DictionaryManager : IDictionaryManager
    {
        private static IDictionary<string, int> data;

        public DictionaryManager()
        {
            data = new Dictionary<string, int>();
        }

        public void AddOrUpdate(string word)
        {
            var keyValuePair = this.FindBy(word);
            if (!string.IsNullOrEmpty(keyValuePair.Key)) data.IncrementCountByOneFor(word);
            else data.Add(word, 1);
        }

        public IEnumerable<KeyValuePair<string, int>> GetAll()
        {
            foreach (var keyValuePair in data)
            {
                yield return keyValuePair;
            }
        }

        public KeyValuePair<string, int> FindBy(string key)
        {
            var returnedKey = from entry in data where data.ContainsKey(key) select entry;
            return returnedKey.FirstOrDefault();
        }

        public void ClearAll()
        {
            data.Clear();
        }
    }
}