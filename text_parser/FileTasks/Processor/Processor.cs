﻿namespace text_parser.FileTasks.Processor
{
    using text_parser.FileTasks.Processor.contracts;
    using text_parser.Infrastructure.contracts;
    using text_parser.Utilities;

    public class Processor : IProcessor
    {
        private readonly IDictionaryManager dictionaryManager;

        public Processor(IDictionaryManager dictionaryManager)
        {
            this.dictionaryManager = dictionaryManager;
        }

        public void Process(string[] words)
        {
            words.ForAll(word => dictionaryManager.AddOrUpdate(word));
        }
    }
}