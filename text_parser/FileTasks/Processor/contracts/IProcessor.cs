﻿namespace text_parser.FileTasks.Processor.contracts
{
    public interface IProcessor
    {
        void Process(string[] words);
    }
}