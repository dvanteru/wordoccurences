﻿namespace text_parser.FileTasks.Reader
{
    using System;
    using System.IO;
    using text_parser.FileTasks.Processor.contracts;
    using text_parser.Repository.Contracts;
    using text_parser.FileTasks.Reader.Contracts;

    public class FileStreamParser : IFileStreamParser
    {
        private readonly IProcessor processor;
        private readonly IFileRepository fileRepository;

        public FileStreamParser(IProcessor processor, IFileRepository fileRepository)
        {
            this.processor = processor;
            this.fileRepository = fileRepository;
        }

        public void Parse(string filePath)
        {
            var memoryStream = this.fileRepository.OpenFile(filePath);
            memoryStream.Seek(0, SeekOrigin.Begin);
            using (var streamReader = new StreamReader(memoryStream))
            {
                while (streamReader.Peek() >= 0)
                {
                   var line = streamReader.ReadLine();
                    this.processor.Process(line.Split(new char[]{' '}, StringSplitOptions.RemoveEmptyEntries));
                }
             }
          }
    }
}
