﻿namespace text_parser.FileTasks.Reader.Contracts
{
    public interface IFileStreamParser
    {
        void Parse(string filePath);
    }
}
