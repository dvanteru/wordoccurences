﻿namespace text_parser_presentation
{
    using text_parser.FileTasks.Processor;
    using text_parser.FileTasks.Reader;
    using text_parser.Infrastructure;
    using text_parser.PrimeTasks;
    using text_parser.Repository;

    public static class ProgramBuilder
    {
        public static Program Build(string[] args)
        {
            var defaultConsole = new DefaultConsole();
            var dictionaryManager = new DictionaryManager();
            dictionaryManager.ClearAll();
            var fileStreamParser = new FileStreamParser(new Processor(dictionaryManager), new FileRepository());
            var primeGenerator = new GeneratePrimes();
            return new Program(fileStreamParser, defaultConsole, dictionaryManager, primeGenerator);
        }
    }
}
