﻿namespace text_parser_presentation
{
    public enum ExitCode
    {
        InitializationFailure = -1,
        Success = 0
    }
}