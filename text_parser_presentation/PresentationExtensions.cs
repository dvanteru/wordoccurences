﻿namespace text_parser_presentation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using text_parser.PrimeTasks;

    public static class PresentationExtensions
    {
        public static IEnumerable<KeyValuePair<string, int>> Sort(this IEnumerable<KeyValuePair<string, int>> dictionary)
        {
            var sortedDictionary = (from entry in dictionary orderby entry.Value descending select entry).AsEnumerable();
            return sortedDictionary;
        }

        public static void ForAll(this IEnumerable<KeyValuePair<string, int>> dictionary, Action<string> action)
        {
            foreach (var item in dictionary)
            {
                action.Invoke(string.Format("{0}  {1} {2}", item.Key, item.Value, item.Value.isPrime()));
            }
        }

        public static string isPrime(this int number)
        {
            var found = GeneratePrimes.GetAllPrimeNumbers().Any(x => x == number);
            return found ? "Prime" : string.Empty;
        }
    }
}