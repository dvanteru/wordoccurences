﻿namespace text_parser_presentation
{
    using System;
    using text_parser.Infrastructure.contracts;
    using text_parser_presentation.contracts;
    using text_parser.FileTasks.Reader.Contracts;
    using System.Linq;
    using text_parser.PrimeTasks.contracts;

    public class Program
    {
        private readonly IFileStreamParser fileStreamParser;

        private readonly IConsole console;

        private readonly IDictionaryManager dictionaryManager;

        private readonly IGeneratePrimes generatePrimes;

        public Program(IFileStreamParser fileStreamParser, IConsole console, IDictionaryManager dictionaryManager, IGeneratePrimes generatePrimes)
        {
            this.fileStreamParser = fileStreamParser;
            this.generatePrimes = generatePrimes;
            this.dictionaryManager = dictionaryManager;
            this.console = console;
        }

        static void Main(string[] args)
        {
            var program = ProgramBuilder.Build(args);
            var exitCode = program == null ? ExitCode.InitializationFailure : program.Run();
            Environment.Exit((int)exitCode);
            Console.ReadKey();
        }

        private ExitCode Run()
        {
            this.fileStreamParser.Parse("test.txt");
            var sortedDictionary = dictionaryManager.GetAll().Sort().Take(100);
            this.generatePrimes.CalculatePrimesUntil(sortedDictionary.FirstOrDefault().Value);
            sortedDictionary.ForAll(word => this.console.WriteLine(word));
            return ExitCode.Success;
        }
    }
}
