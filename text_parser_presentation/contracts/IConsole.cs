﻿namespace text_parser_presentation.contracts
{
    public interface IConsole
    {
        void Write(string line);
        void WriteLine(string line);
    }
}
