﻿namespace text_parser_presentation
{
    using System;
    using text_parser_presentation.contracts;

    public class DefaultConsole : IConsole
    {
        public void Write(string line)
        {
            Console.Write(line);
        }

        public void WriteLine(string line)
        {
            Console.WriteLine(line);
        }
    }
}
